/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author daniel
 */
public class Retencion {

    // private final Float PORCENTAJE_EDUCATIVO = 0.08f;
    // private final Float PORCENTAJE_PROFESIONAL = 0.1f;
    static public Integer contador = 1;
    private Integer nroRetencion;
    private String cedulaCliente;
    private String apellidoCliente;
    private Factura factura;
    private Float valorRetencion;

    public Retencion() {
        nroRetencion = contador;
        contador += 1;
    }

    public Retencion(Integer nroRetencion, String cedulaCliente, String apellidoCliente, Factura factura, Float valorRetencion) {
        this.nroRetencion = nroRetencion;
        this.cedulaCliente = cedulaCliente;
        this.apellidoCliente = apellidoCliente;
        this.factura = factura;
        this.valorRetencion = valorRetencion;
    }

    public Integer getNroRetencion() {
        return nroRetencion;
    }

    public void setNroRetencion(Integer nroRetencion) {
        this.nroRetencion = nroRetencion;
    }

    public String getCedulaCliente() {
        return cedulaCliente;
    }

    public void setCedulaCliente(String cedulaCliente) {
        this.cedulaCliente = cedulaCliente;
    }

    public String getApellidoCliente() {
        return apellidoCliente;
    }

    public void setApellidoCliente(String apellidoCliente) {
        this.apellidoCliente = apellidoCliente;
    }

    public Factura getFactura() {
        return factura;
    }

    public void setFactura(Factura factura) {
        this.factura = factura;

        Double porcentaje = factura.getTipoServicio().getPorcentaje() / 100.00;
        Double retencion = porcentaje * factura.getTotal();

        this.valorRetencion = (float) (Math.round(retencion * 100.0) / 100.0);
        this.cedulaCliente = factura.getCliente().getCedula();
        this.apellidoCliente = factura.getCliente().getApellido();
    }

    public Float getValorRetencion() {
        return valorRetencion;
    }

    @Override
    public String toString() {
        String result = "";

        result += "Retención correspondiente a factura N° " + this.factura.getID() + ": \n";
        result += this.factura.toString() + "\n";
        result += " ---- Retención N° " + nroRetencion + " ---- \n";
        result += "Tipo Servicio: " + this.factura.getTipoServicio().toString() + "\n";
        result += "Porcentaje retención: " + this.factura.getTipoServicio().getPorcentaje() + "\n";
        result += "Valor de retención: " + this.valorRetencion + "\n";

        return result;
    }
}
