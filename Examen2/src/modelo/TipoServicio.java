/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author vivic
 */
public enum TipoServicio {
    EDUCATIVO("Servicio educativo", 8), PROFESIONAL("Servicio profesional", 10);

    private String nombre;
    private Integer porcentaje;

    //Añadir un onstructor
    TipoServicio(String name, Integer percentage) {
        nombre = name;
        porcentaje = percentage;
    }

    public String getNombre() {
        return nombre;
    }

    public Integer getPorcentaje() {
        return porcentaje;
    }

    //Añadir un método
    @Override
    public String toString() {
        return nombre;
    }
}
