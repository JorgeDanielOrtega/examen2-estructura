/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author vivic
 */
public class Factura {

    static public Integer contador = 1;
    private Integer ID;
    private Cliente cliente;
    // private ListaEnlazada<ItemFactura> productos = new ListaEnlazada();
    private Float total;
    private TipoServicio tipoServicio;

    public Factura() {
        ID = contador;
        contador += 1;
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public TipoServicio getTipoServicio() {
        return tipoServicio;
    }

    public void setTipoServicio(TipoServicio tipoServicio) {
        this.tipoServicio = tipoServicio;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Float getTotal() {
        return total;
    }

    public void setTotal(Float total) {
        this.total = total;
    }

    @Override
    public String toString() {
        String result = "";

        result += "---- Factura N° " + ID + " ---- ";
        result += "\nNombre: " + cliente.getNombre();
        result += "\nApellido: " + cliente.getApellido();
        result += "\nCedula: " + cliente.getCedula();

        result += "\nValor factura: " + total;

        return result;
    }

}
