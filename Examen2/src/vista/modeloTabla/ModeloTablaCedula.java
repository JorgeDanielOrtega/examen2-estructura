/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista.modeloTabla;

import controlador.listas.ListaEnlazada;
import javax.swing.table.AbstractTableModel;
import modelo.Factura;
import modelo.Retencion;

/**
 *
 * @author daniel
 */
public class ModeloTablaCedula extends AbstractTableModel {

    ListaEnlazada<Factura> facturaList = new ListaEnlazada<>();

    public ListaEnlazada<Factura> getFacturaList() {
        return facturaList;
    }

    public void setFacturaList(ListaEnlazada<Factura> facturaList) {
        this.facturaList = facturaList;
    }

    @Override
    public int getColumnCount() {

        return 6;

    }

    @Override
    public int getRowCount() {

        return facturaList.getSize();

    }

    @Override
    public String getColumnName(int column) {

        switch (column) {
            case 0:
                return "Nro. Factura";
            case 1:
                return "C.I cliente";
            case 2:
                return "Nombre";
            case 3:
                return "Apellido";
            case 4:
                return "Total";
            case 5:
                return "Tipo Servicio";
            default:
                return null;
        }

    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        Factura factura = null;
        try {

            factura = facturaList.obtener(rowIndex);
        } catch (Exception e) {
        }

        switch (columnIndex) {
            case 0:
                return (factura != null) ? factura.getID().toString() : "NO DEFINIDO";
            case 1:
                return (factura != null) ? factura.getCliente().getCedula() : "NO DEFINIDO";
            case 2:
                return (factura != null) ? factura.getCliente().getNombre() : "NO DEFINIDO";
            case 3:
                return (factura != null) ? factura.getCliente().getApellido() : "NO DEFINIDO";
            case 4:
                return (factura != null) ? factura.getTotal() : "NO DEFINIDO";
            case 5:
                return (factura != null) ? factura.getTipoServicio().getNombre() : "NO DEFINIDO";
            default:
                return null;
        }
    }

}


