/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista.modeloTabla;

import controlador.listas.ListaEnlazada;
import javax.swing.table.AbstractTableModel;
import modelo.Factura;
import modelo.Retencion;

/**
 *
 * @author daniel
 */
public class ModeloTablaRetencion extends AbstractTableModel {

    ListaEnlazada<Retencion> retencionList = new ListaEnlazada<>();

    public ListaEnlazada<Retencion> getRetencionList() {
        return retencionList;
    }

    public void setRetencionList(ListaEnlazada<Retencion> retencionList) {
        this.retencionList = retencionList;
    }

    @Override
    public int getColumnCount() {

        return 4;

    }

    @Override
    public int getRowCount() {

        return retencionList.getSize();

    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "Nro. Retenciones";
            case 1:
                return "C.I";
            case 2:
                return "Apellido";
            case 3:
                return "Nro. Factura";
            default:
                return null;
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Retencion retencion = null;

        try {
            retencion = retencionList.obtener(rowIndex);

        } catch (Exception e) {
        }

        switch (columnIndex) {
            case 0:
                return (retencion != null) ? retencion.getNroRetencion().toString() : "NO DEFINIDO";
            case 1:
                return (retencion != null) ? retencion.getCedulaCliente() : "NO DEFINIDO";
            case 2:
                return (retencion != null) ? retencion.getApellidoCliente().toString() : "NO DEFINIDO";
            case 3:
                return (retencion != null) ? retencion.getFactura().getID().toString() : "NO DEFINIDO";
            default:
                return null;
        }

    }

}
