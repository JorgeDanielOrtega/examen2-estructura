/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilidades;

import java.lang.reflect.Field;
import javax.swing.JComboBox;

/**
 *
 * @author daniel
 */
public class Utilidades {

    private static final Integer A_MINUSCULA = 97;
    private static final Integer Z_MINUSCULA = 122;
    private static final Integer A_MAYUSCULA = 65;
    private static final Integer Z_MAYUSCULA = 90;
    private static final Integer ESPACIO = 32;
    private static final Integer NUM_CERO = 48;
    private static final Integer NUM_NUEVE = 57;

    public static void rellenarCombo(JComboBox cmb, Integer seleccion) {
        cmb.removeAllItems();
        if (seleccion == 1) {
            cmb.addItem("CEDULA");
            cmb.addItem("NRORETENECION");
        }
        else if (seleccion == 2) {
            cmb.addItem("APELLIDO");
            cmb.addItem("CEDULA");
        } 
//else if (seleccion == 3) {
//            cmb.addItem("BINARIO");
//            cmb.addItem("BINARIO-LINEAL");
//        } else if (seleccion == 4) {
//            for (Raza r : Raza.values()) {
//                cmb.addItem(r);
//            }
//        } else if (seleccion == 5) {
//            for (Color c : Color.values()) {
//                cmb.addItem(c);
//            }
//        } else if(seleccion == 6){
//            cmb.addItem("HEMBRA");
//            cmb.addItem("MACHO");
//        }else{
//            cmb.addItem("id");
//            cmb.addItem("nombre");
//            cmb.addItem("raza");
//            cmb.addItem("edadAnios");
//            cmb.addItem("color");
//            cmb.addItem("genero");
//        }
    }

    public static Boolean isNumber(Class clase) {
        return clase.getSuperclass().getSimpleName().equalsIgnoreCase("Number");
    }

    public static Boolean isString(Class clase) {
        return clase.getSimpleName().equalsIgnoreCase("String");
    }

    public static Boolean isCharacter(Class clase) {
        return clase.getSimpleName().equalsIgnoreCase("Character");
    }

    public static Boolean isBoolean(Class clase) {
        return clase.getSimpleName().equalsIgnoreCase("Boolean");
    }

    public static Boolean isPrimitive(Class clase) {
        return clase.isPrimitive();
    }

    public static Boolean isObject(Class clase) {
        return (!isBoolean(clase) && !isCharacter(clase)
                && !isNumber(clase) && !isString(clase) && !isPrimitive(clase)
                && !isEnum(clase));
    }

    public static Boolean isEnum(Class clase) {
        return clase.getSuperclass().getSimpleName().toLowerCase().contains("enum");
    }

    public static Field obtenerAtributo(Class clase, String nombre) {
        Field atributo = null;
        for (Field aux : clase.getDeclaredFields()) {
            if (aux.getName().equalsIgnoreCase(nombre)) {
                atributo = aux;
                break;
            }
        }
        return atributo;
    }

    public static Boolean hayCaracteresNoValidos(String cadena) {
        if (!cadena.trim().equals("")) {
            char[] cadenaChar = cadena.trim().toCharArray();
            for (int i = 0; i < cadenaChar.length; i++) {
                int posicionAscii = (int) cadenaChar[i];
                if (!((posicionAscii >= A_MAYUSCULA && posicionAscii <= Z_MAYUSCULA)
                        || (posicionAscii >= A_MINUSCULA && posicionAscii <= Z_MINUSCULA)
                        || posicionAscii == ESPACIO)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static Object cargarListaJson(Class clazz, String nombreArchivo) {
        return new FileJSON(nombreArchivo).cargar(clazz);
    }

    public static Object guardarJson(Object controlador, String nombreArchivo) {
        return new FileJSON(nombreArchivo).guardar(controlador);
    }

    public static String capitalizar(String cadena) {
        if (!cadena.trim().equalsIgnoreCase("")) {
            char[] cadenaChar = cadena.trim().toCharArray();
            cadenaChar[0] = Character.toUpperCase(cadenaChar[0]);
            for (int i = 0; i < cadenaChar.length; i++) {
                if (cadenaChar[i] == ' ') {
                    cadenaChar[i + 1] = Character.toUpperCase(cadenaChar[i + 1]);
                }
            }
            return String.valueOf(cadenaChar);
        }
        return cadena;
    }

    public static String capitalizarPrimeraPalabra(String cadena) {
        if (!cadena.trim().equalsIgnoreCase("")) {
            char[] cadenaChar = cadena.trim().toCharArray();
            cadenaChar[0] = Character.toUpperCase(cadenaChar[0]);
            return String.valueOf(cadenaChar);
        }
        return cadena;
    }

    public static Boolean hayEspaciosJuntos(String cadena) {
        if (!cadena.trim().equals("")) {
            char[] cadenaChar = cadena.trim().toCharArray();
            for (int i = 0; i < cadenaChar.length; i++) {

                if (cadenaChar[i] == ' ') {
                    if (cadenaChar[i + 1] == ' ') {
                        return true;
                    }
                }
            }
            return false;
        }
        return true;
    }

    public static Boolean hayEspacios(String cadena) {
        if (!cadena.trim().equals("")) {
            char[] cadenaChar = cadena.trim().toCharArray();
            for (int i = 0; i < cadenaChar.length; i++) {
                if (cadenaChar[i] == ' ') {
                    return true;
                }
            }
//            return false;
        }
        return false;
    }

    public static Boolean haySoloLetras(String cadena) {
        if (!cadena.trim().equals("")) {
            char[] cadenaChar = cadena.trim().toCharArray();
            for (int i = 0; i < cadenaChar.length; i++) {
                int posicionAscii = (int) cadenaChar[i];
                if (((posicionAscii >= A_MAYUSCULA && posicionAscii <= Z_MAYUSCULA)
                        || (posicionAscii >= A_MINUSCULA && posicionAscii <= Z_MINUSCULA))) {
                    continue;
                } else {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public static Boolean haySoloLetrasConCharacterPersonalizado(String cadena, char characterPersonalizado) {
        if (!cadena.trim().equals("")) {
            char[] cadenaChar = cadena.trim().toCharArray();
            for (int i = 0; i < cadenaChar.length; i++) {
                int posicionAscii = (int) cadenaChar[i];
                if (((posicionAscii >= A_MAYUSCULA && posicionAscii <= Z_MAYUSCULA)
                        || (posicionAscii >= A_MINUSCULA && posicionAscii <= Z_MINUSCULA)) || posicionAscii == (int) characterPersonalizado) {
                    continue;
                } else {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public static Boolean hayCaracteresNoValidosExcepcionNumeros(String cadena) {
        if (!cadena.trim().equals("")) {
            char[] cadenaChar = cadena.trim().toCharArray();
            for (int i = 0; i < cadenaChar.length; i++) {
                int posicionAscii = (int) cadenaChar[i];
                if ((posicionAscii >= NUM_CERO && posicionAscii <= NUM_NUEVE)) {
                    continue;
                }
                if (!((posicionAscii >= A_MAYUSCULA && posicionAscii <= Z_MAYUSCULA)
                        || (posicionAscii >= A_MINUSCULA && posicionAscii <= Z_MINUSCULA)
                        || posicionAscii == ESPACIO)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static Boolean haySoloNumeros(String cadena) {
        if (!cadena.trim().equals("")) {
            char[] cadenaChar = cadena.trim().toCharArray();
            for (int i = 0; i < cadenaChar.length; i++) {
                int posicionAscii = (int) cadenaChar[i];
                if (!(posicionAscii >= NUM_CERO && posicionAscii <= NUM_NUEVE)) {
                    return false;
                }
            }
        }
        return true;
    }

    public static Boolean hayNPalabras(String cadena, int maxpalabras) {
        if (!cadena.equals("") && maxpalabras > 0) {
            char[] cadenaChar = cadena.trim().toCharArray();
            int numEspacios = 0;
            for (int i = 0; i < cadenaChar.length; i++) {
                if (cadenaChar[i] == ' ') {
                    numEspacios++;
                }
            }
            return (numEspacios + 1) == maxpalabras;
        }
        return false;
    }

    public static float generarNumeroAleatorio(Integer minimo, Integer maximo) {
        // generamos numero
        double random = (Math.random() * (maximo - minimo) + minimo);
        // lo redondeamos
        random = Math.round(random * 100.0) / 100.0;

        return (float) random;
    }

}
