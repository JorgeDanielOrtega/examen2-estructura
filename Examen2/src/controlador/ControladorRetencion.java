/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import controlador.listas.ListaEnlazada;
import modelo.Factura;
import modelo.Retencion;

/**
 *
 * @author daniel
 */
public class ControladorRetencion {

    private ListaEnlazada<Retencion> retencionList = new ListaEnlazada<>();
    private ListaEnlazada<Retencion> retencionListTemp = new ListaEnlazada<>();

    public ControladorRetencion() {
    }

    public void ordenarRetenciones(Integer option) throws Exception {
        if (option == 1) {
            retencionList.ordenacionShell(2, "cedulaCliente");
        } else {
            retencionList.ordenacionShell(2, "nroRetencion");
        }
    }

    public ListaEnlazada<Retencion> buscarRetenciones(Integer option, String dato) throws Exception { // cambiar el objeto que devuelve quizas

        if (option == 1) {
            retencionListTemp = retencionList.busquedaLineal(new Retencion(null, null, dato, null, null), false, "apellidoCliente");
        } else {
            retencionListTemp = retencionList.busquedaLineal(new Retencion(null, dato, null, null, null), false, "cedulaCliente");
        }
        
        return retencionListTemp;
    }

    public void registrarRetencion(String cedulaCliente, String apellidocliente, Factura factura) {
        Retencion rTemp = new Retencion(retencionList.getSize() + 1, cedulaCliente, apellidocliente, factura, null); //poner mas tarde el valor de la factura para la retencions
        retencionList.insertar(rTemp);
    }

    public ListaEnlazada<Retencion> getRetencionList() {
        return retencionList;
    }

    public void setRetencionList(ListaEnlazada<Retencion> retencionList) {
        this.retencionList = retencionList;
    }

    public ListaEnlazada<Retencion> getRetencionListTemp() {
        return retencionListTemp;
    }

    public void setRetencionListTemp(ListaEnlazada<Retencion> retencionListTemp) {
        this.retencionListTemp = retencionListTemp;
    }
    
    

}
