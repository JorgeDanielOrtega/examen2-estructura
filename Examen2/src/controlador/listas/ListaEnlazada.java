package controlador.listas;

import utilidades.Utilidades;
import controlador.listas.excepciones.BusquedaNulaException;
import controlador.listas.excepciones.ListIsVoidException;
import controlador.listas.excepciones.ListOutLimitException;
import excepciones.AtributoExcepcion;
import java.lang.reflect.Array;
import java.lang.reflect.Field;

public class ListaEnlazada<E> { //Lista de elementos

    private NodoLista<E> cabecera;
    private Integer size;
    private static final Integer DESCENDENTE = 2;
//    private NodoLista cola; //no se requiere cola cuando son listas enlazadas, la cola se la necesita cuando son listas circulares(listas que no apuntan a null) o cuando son doblemente enlazadas

    public ListaEnlazada() {
        cabecera = null;
        size = 0;
    }

    public int getUltimaPosicionOcupada() {
        int posicion = 0;

        if (!isVoid()) {
            NodoLista<E> aux = cabecera;
            posicion += 1;

            while (aux.getSiguiente() != null) {
                posicion += 1;
                aux = aux.getSiguiente();
            }
        }

        return posicion;
    }

    //1 metodo
    public Boolean isVoid() {
        return cabecera == null;
    }

//    public Integer size(){
//        //Este metodo no esta bien semanticamente(cuando se corre el programa), si se lo analiza de forma algoritmica, este metodo no nos conviene
//        Integer size = 0;
//        NodoLista<E> aux = cabecera;
//        while (aux != null) {            
//            size ++; 
//            aux = aux.getSiguiente();
//        }msg
//        return size;
//    }
    //2 metodo
    public void insertar(E dato) {
        //No se sabe cual va a ser el siguiente
        NodoLista<E> node = new NodoLista<E>(dato, null);
        if (isVoid()) {
            this.cabecera = node;
        } else {
            NodoLista<E> aux = cabecera;
            while (aux.getSiguiente() != null) {
                aux = aux.getSiguiente();
            }
            aux.setSiguiente(node);
        }
        this.size++;
    }

    //2.1 metodo
    public void insertarCabecera(E dato) {
        if (isVoid()) {
            insertar(dato);
        } else {
            NodoLista<E> node = new NodoLista<E>(dato, null);
            node.setSiguiente(cabecera);
            this.cabecera = node;
            this.size++;
        }
    }

    //2.2 metodo
    public void insertarPosition(E dato, Integer pos) throws ListOutLimitException {
        if (isVoid()) {
            insertar(dato);
        } else if (pos == 0) {
            insertarCabecera(dato);
        } else if (pos > 0 && pos < size) {
            NodoLista<E> aux = cabecera;
//            NodoLista<E> node = new NodoLista<>(dato, null);
            for (int i = 0; i < pos - 1; i++) {
                aux = aux.getSiguiente();
            }

            NodoLista<E> node = new NodoLista<>(dato, aux.getSiguiente());
            aux.setSiguiente(node);

//            NodoLista<E> siguiente = aux.getSiguiente();
//            aux.setSiguiente(node);
//            node.setSiguiente(siguiente);
            size++;

        } else if (pos == size) {
            insertar(dato);
        } else {
            throw new ListOutLimitException(pos);
        }
    }

    //3 metodo
    public void print() {
        NodoLista<E> aux = cabecera;
        System.out.println("---------------------LISTA ENLAZADA------------------");
        while (aux != null) {
            System.out.println(aux.getDato().toString() + "    ");
            aux = aux.getSiguiente();
        }
        System.out.println("-----------------------------------------------------");
    }

    public E obtener(Integer pos) throws ListIsVoidException, ListOutLimitException {
        E dato = null;
        if (isVoid()) {
            throw new ListIsVoidException();
        } else if (pos >= 0 && pos < size) {
            NodoLista<E> aux = cabecera;
            for (int i = 0; i < pos; i++) {
                aux = aux.getSiguiente();
            }
            dato = aux.getDato();
        } else {
            throw new ListOutLimitException(pos);
        }
        return dato;
    }

    public void modificarPosicion(E dato, Integer pos) throws ListIsVoidException, ListOutLimitException {
        if (isVoid()) {
            throw new ListIsVoidException();
        } else if (pos >= 0 && pos < size) {
            NodoLista<E> aux = cabecera;
            for (int i = 0; i < pos; i++) {
                aux = aux.getSiguiente();
            }
            aux.setDato(dato);
        } else {
            throw new ListOutLimitException(pos);
        }
    }

    public void modificarCabecera(E dato) throws ListIsVoidException {
        if (isVoid()) {
            throw new ListIsVoidException();
        }
        cabecera.setDato(dato);
    }

    public void modificarUltimo(E dato) throws ListIsVoidException {
        if (isVoid()) {
            throw new ListIsVoidException();
        }
        NodoLista<E> aux = cabecera;
        while (aux.getSiguiente() != null) {
            aux = aux.getSiguiente();
        }
        aux.setDato(dato);
    }

    public void eliminarPosicion(Integer pos) throws ListIsVoidException, ListOutLimitException {
        if (isVoid()) {
            throw new ListIsVoidException();
        } else if (pos > 0 && pos < size) {
            NodoLista<E> aux = cabecera;

            for (int i = 0; i < pos - 1; i++) {
                aux = aux.getSiguiente();
            }
            NodoLista<E> siguiente = aux.getSiguiente();
            aux.setSiguiente(siguiente.getSiguiente());
            siguiente.setSiguiente(null);
            size--;
        } else if (pos == 0) {
            eliminarCabecera();
        } else {
            throw new ListOutLimitException(pos);
        }
    }

    public void eliminarCabecera() throws ListIsVoidException {
        if (isVoid()) {
            throw new ListIsVoidException();
        }
        if (size == 1) {
            cabecera = null;
        } else {
            NodoLista<E> aux = cabecera.getSiguiente();
            cabecera.setSiguiente(null);
            cabecera = aux;
        }
        size--;
    }

    public void eliminarUltimo() throws ListIsVoidException {
        if (isVoid()) {
            throw new ListIsVoidException();
        }

        if (size == 1) {
            eliminarCabecera();
        } else {
            NodoLista<E> aux = cabecera;
            while (aux.getSiguiente().getSiguiente() != null) {
                aux = aux.getSiguiente();
            }
            aux.setSiguiente(null);
            size--;
        }
    }

    public E[] toArray() {
        E[] matriz = null;

        if (this.size > 0) {
            matriz = (E[]) Array.newInstance(cabecera.getDato().getClass(), this.size);
            NodoLista<E> aux = cabecera;
            for (int i = 0; i < size; i++) {
                matriz[i] = aux.getDato();
                aux = aux.getSiguiente();
            }
        }

        return matriz;
    }

    public ListaEnlazada<E> toList(E[] array) {
        try {

            if (array.length > 0) {
                for (int i = 0; i < array.length; i++) {
                    this.modificarPosicion(array[i], i);
                }
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }

        return this;
    }

    public ListaEnlazada<E> toListDesdeCero(E[] array) {
        this.vaciar();
        for (int i = 0; i < array.length; i++) {
            this.insertar(array[i]);
        }
        return this;
    }

    private ListaEnlazada<E> toListDesdeRango(int i, int s, E[] array) {
        this.vaciar();
        for (int j = i; j <= s; j++) {
            insertar(array[j]);
        }
        return this;
    }

    public void vaciar() {
        this.cabecera = null;
        this.size = 0;
    }

    public ListaEnlazada<E> burbuja(String atributo, Integer tipoOrdenacion) throws Exception {
        Class<E> clazz = null;
        E[] array = toArray();
        if (size > 0) {
            clazz = (Class<E>) cabecera.getDato().getClass();
            Boolean isObject = Utilidades.isObject(clazz);

            for (int i = array.length; i > 1; i--) { //Verificar si esta bien
                for (int j = 0; j < i - 1; j++) {
                    if (Utilidades.isObject(clazz)) {
                        intercambioObjeto(j, array, clazz, tipoOrdenacion, atributo);
                    } else {

                        intercambioDato(j, array, tipoOrdenacion);
                    }
                }
            }
        }

        if (array != null) {
            toList(array);
        }
        return this;
    }

    private void intercambioObjeto(int j, E[] matriz, Class clazz, Integer tipoOrdenacion, String atributo) throws Exception {
        E auxj = matriz[j];
        E auxj1 = matriz[j + 1];
        Field field = Utilidades.obtenerAtributo(clazz, atributo); // Agregar el parametro atributo, y completar el metodo obtener atributo
        if (field == null) {
            throw new AtributoExcepcion();
        }
        field.setAccessible(true);
        Object a = field.get(auxj);
        Object b = field.get(auxj1);
        intercambio(j, matriz, a, b, tipoOrdenacion);

    }

    private void intercambioDato(int j, E[] matriz, Integer tipoOperacion) {
        E auxj = matriz[j];
        E auxj1 = matriz[j + 1];

        intercambio(j, matriz, auxj, auxj1, tipoOperacion);
    }

    private void intercambio(int j, E[] matriz, Object auxj, Object auxj1, Integer tipoOperacion) {
        Class clazz = auxj.getClass();
        E a = matriz[j];
        E b = matriz[j + 1];

        if (Utilidades.isNumber(clazz)) {
            if (tipoOperacion == DESCENDENTE) {
                if (((Number) auxj).doubleValue() < ((Number) auxj1).doubleValue()) {
                    matriz[j] = b;
                    matriz[j + 1] = a;
                }
            } else {
                if (((Number) auxj).doubleValue() > ((Number) auxj1).doubleValue()) {
                    matriz[j] = b;
                    matriz[j + 1] = a;
                }
            }

        }

        if (Utilidades.isString(clazz)) {
            if (tipoOperacion == DESCENDENTE) {
                if (((String) auxj).compareToIgnoreCase((String) auxj1) < 0) {
                    matriz[j] = b;
                    matriz[j + 1] = a;
                }
            } else {
                if (((String) auxj).compareToIgnoreCase((String) auxj1) > 0) {
                    matriz[j] = b;
                    matriz[j + 1] = a;
                }
            }

        }
        if (Utilidades.isCharacter(clazz)) {
            if (tipoOperacion == DESCENDENTE) {
                if (((Character) auxj).toString().compareToIgnoreCase(((Character) auxj1).toString()) < 0) {
                    matriz[j] = b;
                    matriz[j + 1] = a;
                }
            } else {
                if (((Character) auxj).toString().compareToIgnoreCase(((Character) auxj1).toString()) > 0) {
                    matriz[j] = b;
                    matriz[j + 1] = a;
                }
            }
        }

        if (Utilidades.isEnum(clazz)) {
            if (tipoOperacion == DESCENDENTE) {
                if (((Enum) auxj).compareTo(((Enum) auxj1)) < 0) {
                    matriz[j] = b;
                    matriz[j + 1] = a;
                }
            } else {
                if (((Enum) auxj).compareTo(((Enum) auxj1)) > 0) {
                    matriz[j] = b;
                    matriz[j + 1] = a;
                }
            }
        }

    }

    public ListaEnlazada<E> ordenacionSeleccion(String atributo, Integer tipoOrdenacion) throws Exception {
        Class<E> clazz = null;
        E[] array = toArray();
        if (size > 0) {
            clazz = (Class<E>) cabecera.getDato().getClass();
            Boolean isObject = Utilidades.isObject(clazz);
            //comienza algoritmo seleccion
            Integer i, j, k = 0;
            Integer n = array.length;
            E t = null;
            for (i = 0; i < n - 1; i++) { //Verificar si esta bien
                k = i;
                t = array[i];
                for (j = i + 1; j < n; j++) {
                    E auxj1 = array[j];
                    Object[] aux = null;
                    if (isObject) {
                        aux = evaluarCambioObjeto(t, auxj1, j, tipoOrdenacion, clazz, atributo);
                    } else {
                        aux = evaluarCambioDato(t, auxj1, j, tipoOrdenacion);

                    }

                    if (aux[0] != null) {
                        t = (E) aux[0];
                        k = (Integer) aux[1];
                    }

                }
                array[k] = array[i];
                array[i] = t;
            }
        }

        if (array != null) {
            toList(array);
        }
        return this;
    }

    private Object[] evaluarCambioDato(E auxj, E auxj1, Integer j, Integer tipoOrdenacion) {
        return evaluarCambio(auxj, auxj1, auxj1, j, tipoOrdenacion);
    }

    private Object[] evaluarCambioObjeto(E auxj, E auxj1, Integer j, Integer tipoOrdenacion, Class clazz, String atributo) throws Exception {
        Field field = Utilidades.obtenerAtributo(clazz, atributo); // Agregar el parametro atributo, y completar el metodo obtener atributo
        if (field == null) {
            throw new AtributoExcepcion();
        }
        field.setAccessible(true);
        Object a = field.get(auxj);
        Object b = field.get(auxj1);
        return evaluarCambio(a, b, auxj1, j, tipoOrdenacion);
    }

    private Object[] evaluarCambio(Object auxj, Object auxj1, E dato, Integer j, Integer tipoOrdenacion) {
        Object[] aux = new Object[2];
        Class clazz = auxj.getClass();
        if (Utilidades.isNumber(clazz)) {
            if (tipoOrdenacion == DESCENDENTE) {
                if (((Number) auxj).doubleValue() < ((Number) auxj1).doubleValue()) {
                    aux[0] = dato;
                    aux[1] = j;
                }
            } else {
                if (((Number) auxj).doubleValue() > ((Number) auxj1).doubleValue()) {
                    aux[0] = dato;
                    aux[1] = j;
                }
            }

        }

        if (Utilidades.isString(clazz)) {
            if (tipoOrdenacion == DESCENDENTE) {
                if (((String) auxj).compareToIgnoreCase((String) auxj1) < 0) {
                    aux[0] = dato;
                    aux[1] = j;
                }
            } else {
                if (((String) auxj).compareToIgnoreCase((String) auxj1) > 0) {
                    aux[0] = dato;
                    aux[1] = j;
                }
            }

        }
        if (Utilidades.isCharacter(clazz)) {
            if (tipoOrdenacion == DESCENDENTE) {
                if (((Character) auxj).toString().compareToIgnoreCase(((Character) auxj1).toString()) < 0) {
                    aux[0] = dato;
                    aux[1] = j;
                }
            } else {
                if (((Character) auxj).toString().compareToIgnoreCase(((Character) auxj1).toString()) > 0) {
                    aux[0] = dato;
                    aux[1] = j;
                }
            }
        }
        if (Utilidades.isEnum(clazz)) {
            if (tipoOrdenacion == DESCENDENTE) {
                if (((Enum) auxj).compareTo(((Enum) auxj1)) < 0) {
                    aux[0] = dato;
                    aux[1] = j;
                }
            } else {
                if (((Enum) auxj).compareTo(((Enum) auxj1)) > 0) {
                    aux[0] = dato;
                    aux[1] = j;
                }
            }
        }
        return aux;
    }

    // ------------------------------- quick sort -------------------------------
    public ListaEnlazada<E> ordenarQuickSort(Integer tipoOrdenacion, String atributo) throws Exception {
        E[] array = toArray();
        array = evaluarLados(array, tipoOrdenacion, atributo);
        this.toListDesdeCero(array);
        return this;
    }

    public E[] evaluarLados(E[] array, Integer tipoOrdenacion, String atributo) throws Exception {
        if (array.length < 2) {
            return array;
        } else {
            ListaEnlazada<E> izq = new ListaEnlazada<>();
            ListaEnlazada<E> der = new ListaEnlazada<>();
            E valorPivote = array[0];
            Object pivoteTemp = valorPivote;
            Object dato = null;
            for (int i = 1; i < array.length; i++) {
                dato = array[i];
                if (Utilidades.isObject(array[0].getClass())) {
                    Class clazz = getCabecera().getDato().getClass();
                    Field campo = Utilidades.obtenerAtributo(clazz, atributo);
                    if (campo == null) {
                        throw new AtributoExcepcion();
                    }
                    campo.setAccessible(true);

                    dato = (Object) campo.get(array[i]);
                    pivoteTemp = (Object) campo.get(array[0]);
                }
                if (Utilidades.isNumber(dato.getClass())) {
                    if (tipoOrdenacion == DESCENDENTE) {
                        if (((Number) dato).doubleValue() > ((Number) pivoteTemp).doubleValue()) {
                            izq.insertar(array[i]);
                        } else {
                            der.insertar(array[i]);
                        }
                    } else {
                        if (((Number) dato).doubleValue() < ((Number) pivoteTemp).doubleValue()) {
                            izq.insertar(array[i]);
                        } else {
                            der.insertar(array[i]);
                        }
                    }
                } else if (Utilidades.isString(dato.getClass())) {
                    if (tipoOrdenacion == DESCENDENTE) {
                        if (((String) dato).compareToIgnoreCase((String) pivoteTemp) > 0) {
                            izq.insertar(array[i]);
                        } else {
                            der.insertar(array[i]);
                        }
                    } else {
                        if (((String) dato).compareToIgnoreCase((String) pivoteTemp) < 0) {
                            izq.insertar(array[i]);
                        } else {
                            der.insertar(array[i]);
                        }
                    }
                } else if (Utilidades.isCharacter(dato.getClass())) {
                    if (tipoOrdenacion == DESCENDENTE) {
                        if (((Character) dato).toString().compareToIgnoreCase(((Character) pivoteTemp).toString()) > 0) {
                            izq.insertar(array[i]);
                        } else {
                            der.insertar(array[i]);
                        }
                    } else {
                        if (((Character) dato).toString().compareToIgnoreCase(((Character) pivoteTemp).toString()) < 0) {
                            izq.insertar(array[i]);
                        } else {
                            der.insertar(array[i]);
                        }
                    }
                } else if (Utilidades.isEnum(dato.getClass())) {
                    if (tipoOrdenacion == DESCENDENTE) {
                        if (((Enum) dato).compareTo(((Enum) pivoteTemp)) > 0) {
                            izq.insertar(array[i]);
                        } else {
                            der.insertar(array[i]);
                        }
                    } else {
                        if (((Enum) dato).compareTo(((Enum) pivoteTemp)) < 0) {
                            izq.insertar(array[i]);
                        } else {
                            der.insertar(array[i]);
                        }
                    }

                }
            }
            ListaEnlazada<E> concatenacion = new ListaEnlazada<>();
            if (izq.getCabecera() != null) {
                E[] izqArray = evaluarLados(izq.toArray(), tipoOrdenacion, atributo);
                concatenacion.toListDesdeCero(izqArray);

            }
            concatenacion.insertar(valorPivote);
            if (der.getCabecera() != null) {
                E[] derArray = evaluarLados(der.toArray(), tipoOrdenacion, atributo);
                concatenacion.insertarArreglo(derArray);
            }
            return concatenacion.toArray();
        }
    }
    // ------------------------------ ORDENACION POR INSERCCION ------------------------------

    public ListaEnlazada<E> ordenacionInserccion(Integer tipoOrdenacion, String atributo) throws Exception {
        E[] array = toArray();
        E valorInserccion = null;
        Integer indiceInserccion = null;
        Object valorComparacion1 = null;
        Object valorComparacion2 = null;
        for (int i = 1; i < array.length; i++) {
            valorInserccion = array[i];
            indiceInserccion = i;
            valorComparacion1 = valorInserccion;
            valorComparacion2 = (indiceInserccion > 0) ? array[indiceInserccion - 1] : array[indiceInserccion]; // corregir mas tarde

            if (Utilidades.isObject(valorComparacion1.getClass())) {
                Class clazz = getCabecera().getDato().getClass();
                Field campo = Utilidades.obtenerAtributo(clazz, atributo);
                if (campo == null) {
                    throw new AtributoExcepcion();
                }
                campo.setAccessible(true);
                valorComparacion1 = campo.get(array[i]);
                valorComparacion2 = (indiceInserccion > 0) ? campo.get(array[indiceInserccion - 1]) : campo.get(array[indiceInserccion]); //ver mas tarde
            }

            if (Utilidades.isNumber(valorComparacion1.getClass())) {
                if (tipoOrdenacion == DESCENDENTE) {
                    while (indiceInserccion > 0
                            && ((Number) valorComparacion1).doubleValue() > ((Number) valorComparacion2).doubleValue()) {
                        array[indiceInserccion] = array[indiceInserccion - 1];
                        array[indiceInserccion - 1] = valorInserccion;
                        indiceInserccion -= 1;
                        if (indiceInserccion > 0) {
                            valorComparacion2 = array[indiceInserccion - 1];
                            valorComparacion2 = tratarObject(valorComparacion2.getClass(), atributo, valorComparacion2);
                        }
                    }
                } else {
                    while (indiceInserccion > 0
                            && ((Number) valorComparacion1).doubleValue() < ((Number) valorComparacion2).doubleValue()) {
                        array[indiceInserccion] = array[indiceInserccion - 1];
                        array[indiceInserccion - 1] = valorInserccion;
                        indiceInserccion -= 1;
                        if (indiceInserccion > 0) {
                            valorComparacion2 = array[indiceInserccion - 1];
                            valorComparacion2 = tratarObject(valorComparacion2.getClass(), atributo, valorComparacion2);
                        }
                    }
                }
            } else if (Utilidades.isString(valorComparacion1.getClass())) {
                if (tipoOrdenacion == DESCENDENTE) {
                    while (indiceInserccion > 0
                            && ((String) valorComparacion1).compareToIgnoreCase((String) valorComparacion2) > 0) {
                        array[indiceInserccion] = array[indiceInserccion - 1];
                        array[indiceInserccion - 1] = valorInserccion;
                        indiceInserccion -= 1;
                        if (indiceInserccion > 0) {
                            valorComparacion2 = array[indiceInserccion - 1];
                            valorComparacion2 = tratarObject(valorComparacion2.getClass(), atributo, valorComparacion2);
                        }
                    }
                } else {
                    while (indiceInserccion > 0
                            && ((String) valorComparacion1).compareToIgnoreCase((String) valorComparacion2) < 0) {
                        array[indiceInserccion] = array[indiceInserccion - 1];
                        array[indiceInserccion - 1] = valorInserccion;
                        indiceInserccion -= 1;
                        if (indiceInserccion > 0) {
                            valorComparacion2 = array[indiceInserccion - 1];
                            valorComparacion2 = tratarObject(valorComparacion2.getClass(), atributo, valorComparacion2);
                        }
                    }
                }
            } else if (Utilidades.isCharacter(valorComparacion1.getClass())) {
                if (tipoOrdenacion == DESCENDENTE) {
                    while (indiceInserccion > 0
                            && ((Character) valorComparacion1).toString().compareToIgnoreCase(((Character) valorComparacion2).toString()) > 0) {
                        array[indiceInserccion] = array[indiceInserccion - 1];
                        array[indiceInserccion - 1] = valorInserccion;
                        indiceInserccion -= 1;
                        if (indiceInserccion > 0) {
                            valorComparacion2 = array[indiceInserccion - 1];
                            valorComparacion2 = tratarObject(valorComparacion2.getClass(), atributo, valorComparacion2);
                        }
                    }
                } else {
                    while (indiceInserccion > 0
                            && ((Character) valorComparacion1).toString().compareToIgnoreCase(((Character) valorComparacion2).toString()) < 0) {
                        array[indiceInserccion] = array[indiceInserccion - 1];
                        array[indiceInserccion - 1] = valorInserccion;
                        indiceInserccion -= 1;
                        if (indiceInserccion > 0) {
                            valorComparacion2 = array[indiceInserccion - 1];
                            valorComparacion2 = tratarObject(valorComparacion2.getClass(), atributo, valorComparacion2);
                        }
                    }
                }
            } else if (Utilidades.isEnum(valorComparacion1.getClass())) {
                if (tipoOrdenacion == DESCENDENTE) {
                    while (indiceInserccion > 0
                            && ((Enum) valorComparacion1).compareTo((Enum) valorComparacion2) > 0) {
                        array[indiceInserccion] = array[indiceInserccion - 1];
                        array[indiceInserccion - 1] = valorInserccion;
                        indiceInserccion -= 1;
                        if (indiceInserccion > 0) {
                            valorComparacion2 = array[indiceInserccion - 1];
                            valorComparacion2 = tratarObject(valorComparacion2.getClass(), atributo, valorComparacion2);
                        }
                    }
                } else {
                    while (indiceInserccion > 0
                            && ((Enum) valorComparacion1).compareTo((Enum) valorComparacion2) < 0) {
                        array[indiceInserccion] = array[indiceInserccion - 1];
                        array[indiceInserccion - 1] = valorInserccion;
                        indiceInserccion -= 1;
                        if (indiceInserccion > 0) {
                            valorComparacion2 = array[indiceInserccion - 1];
                            valorComparacion2 = tratarObject(valorComparacion2.getClass(), atributo, valorComparacion2);
                        }
                    }
                }
            }
        }
        this.toListDesdeCero(array);
        return this;
    }

    // ----------------------------- SHELL -----------------------------
    public ListaEnlazada<E> ordenacionShell(Integer tipoOrdenacion, String atributo) throws Exception {
        E[] array = toArray();
        Integer intervalo = array.length / 2;
        E valorInserccion = null;
        Integer indiceInserccion = null;
        Object valorComparacion1 = null;
        Object valorComparacion2 = null;

        while (intervalo > 0) {
            for (int i = intervalo; i < array.length; i++) {
                valorInserccion = array[i];
                indiceInserccion = i;
                valorComparacion1 = valorInserccion;
                valorComparacion2 = array[indiceInserccion - intervalo];

                if (Utilidades.isObject(valorComparacion1.getClass())) {
                    Class clazz = getCabecera().getDato().getClass();
                    Field campo = Utilidades.obtenerAtributo(clazz, atributo);
                    if (campo == null) {
                        throw new AtributoExcepcion();
                    }
                    campo.setAccessible(true);
                    valorComparacion1 = campo.get(array[i]);
                    valorComparacion2 = campo.get(array[indiceInserccion - intervalo]);
                }

                if (Utilidades.isNumber(valorComparacion1.getClass())) {
                    if (tipoOrdenacion == DESCENDENTE) {
                        while (indiceInserccion >= intervalo
                                && ((Number) valorComparacion1).doubleValue() > ((Number) valorComparacion2).doubleValue()) {
                            array[indiceInserccion] = array[indiceInserccion - intervalo];
                            indiceInserccion -= intervalo;
                            if (indiceInserccion >= intervalo) {
                                valorComparacion2 = array[indiceInserccion - intervalo];
                                valorComparacion2 = tratarObject(valorComparacion2.getClass(), atributo, valorComparacion2);
                            }
                        }
                    } else {
                        while (indiceInserccion >= intervalo
                                && ((Number) valorComparacion1).doubleValue() < ((Number) valorComparacion2).doubleValue()) {
                            array[indiceInserccion] = array[indiceInserccion - intervalo];
                            indiceInserccion -= intervalo;
                            if (indiceInserccion >= intervalo) {
                                valorComparacion2 = array[indiceInserccion - intervalo];
                                valorComparacion2 = tratarObject(valorComparacion2.getClass(), atributo, valorComparacion2);
                            }
                        }
                    }
                } else if (Utilidades.isString(valorComparacion1.getClass())) {
                    if (tipoOrdenacion == DESCENDENTE) {
                        while (indiceInserccion >= intervalo
                                && ((String) valorComparacion1).compareToIgnoreCase((String) valorComparacion2) > 0) {
                            array[indiceInserccion] = array[indiceInserccion - intervalo];
                            indiceInserccion -= intervalo;
                            if (indiceInserccion >= intervalo) {
                                valorComparacion2 = array[indiceInserccion - intervalo];
                                valorComparacion2 = tratarObject(valorComparacion2.getClass(), atributo, valorComparacion2);
                            }
                        }
                    } else {
                        while (indiceInserccion >= intervalo
                                && ((String) valorComparacion1).compareToIgnoreCase((String) valorComparacion2) < 0) {
                            array[indiceInserccion] = array[indiceInserccion - intervalo];
                            indiceInserccion -= intervalo;
                            if (indiceInserccion >= intervalo) {
                                valorComparacion2 = array[indiceInserccion - intervalo];
                                valorComparacion2 = tratarObject(valorComparacion2.getClass(), atributo, valorComparacion2);
                            }
                        }
                    }
                } else if (Utilidades.isCharacter(valorComparacion1.getClass())) {
                    if (tipoOrdenacion == DESCENDENTE) {
                        while (indiceInserccion >= intervalo
                                && ((Character) valorComparacion1).toString().compareToIgnoreCase(((Character) valorComparacion2).toString()) > 0) {
                            array[indiceInserccion] = array[indiceInserccion - intervalo];
                            indiceInserccion -= intervalo;
                            if (indiceInserccion >= intervalo) {
                                valorComparacion2 = array[indiceInserccion - intervalo];
                                valorComparacion2 = tratarObject(valorComparacion2.getClass(), atributo, valorComparacion2);
                            }
                        }
                    } else {
                        while (indiceInserccion >= intervalo
                                && ((Character) valorComparacion1).toString().compareToIgnoreCase(((Character) valorComparacion2).toString()) < 0) {
                            array[indiceInserccion] = array[indiceInserccion - intervalo];
                            indiceInserccion -= intervalo;
                            if (indiceInserccion >= intervalo) {
                                valorComparacion2 = array[indiceInserccion - intervalo];
                                valorComparacion2 = tratarObject(valorComparacion2.getClass(), atributo, valorComparacion2);
                            }
                        }
                    }
                } else if (Utilidades.isEnum(valorComparacion1.getClass())) {
                    if (tipoOrdenacion == DESCENDENTE) {
                        while (indiceInserccion >= intervalo
                                && ((Enum) valorComparacion1).compareTo((Enum) valorComparacion2) > 0) {
                            array[indiceInserccion] = array[indiceInserccion - intervalo];
                            indiceInserccion -= intervalo;
                            if (indiceInserccion >= intervalo) {
                                valorComparacion2 = array[indiceInserccion - intervalo];
                                valorComparacion2 = tratarObject(valorComparacion2.getClass(), atributo, valorComparacion2);
                            }
                        }
                    } else {
                        while (indiceInserccion >= intervalo
                                && ((Enum) valorComparacion1).compareTo((Enum) valorComparacion2) < 0) {
                            array[indiceInserccion] = array[indiceInserccion - intervalo];
                            indiceInserccion -= intervalo;
                            if (indiceInserccion >= intervalo) {
                                valorComparacion2 = array[indiceInserccion - intervalo];
                                valorComparacion2 = tratarObject(valorComparacion2.getClass(), atributo, valorComparacion2);
                            }
                        }
                    }
                }

                array[indiceInserccion] = valorInserccion;
            }
            intervalo /= 2;
        }

        this.toListDesdeCero(array);
        return this;
    }

    private Object tratarObject(Class clazz, String atributo, Object valorComparacion2) throws Exception {
        if (Utilidades.isObject(clazz)) {
//            System.out.println("valro comp 2");
            Field campo = Utilidades.obtenerAtributo(clazz, atributo);
            if (campo == null) {
                throw new AtributoExcepcion();
            }
            campo.setAccessible(true);
            valorComparacion2 = campo.get(valorComparacion2);
        }
        return valorComparacion2;
    }

    // ------------------------------- BUSQUEDA LINEAL -----------------------------
    public ListaEnlazada<E> busquedaLineal(Object elementoBusqueda, Boolean retornarPos, String atributo) throws Exception {
        // en este algortimo no se necesita ordenar
        ListaEnlazada<E> listaConBusqueda = null;
        ListaEnlazada<Integer> listaDePos = null;
        E[] array = this.toArray();
        if (size > 0) {
            for (int i = 0; i < array.length; i++) {
                if (retornarPos) {
                    if (i == 0) {
                        listaDePos = new ListaEnlazada<>();
                    }

                    if (Utilidades.isObject(elementoBusqueda.getClass())) {
                        Class clazz = getCabecera().getDato().getClass();
                        Field campo = Utilidades.obtenerAtributo(clazz, atributo);
                        if (campo == null) {
                            throw new AtributoExcepcion();
                        }
                        campo.setAccessible(true);
                        System.out.println(campo);

                        if (evaluarElemento(campo.get(elementoBusqueda), (E) campo.get(array[i]), false)) {
                            listaDePos.insertar(i);
                        }
                    }

                    if (evaluarElemento(elementoBusqueda, array[i], false)) {
                        listaDePos.insertar(i);
                    }

                } else {
                    if (i == 0) {
                        listaConBusqueda = new ListaEnlazada<>();
                    }

                    if (Utilidades.isObject(elementoBusqueda.getClass())) {
                        Class clazz = getCabecera().getDato().getClass();
                        Field campo = Utilidades.obtenerAtributo(clazz, atributo);
                        if (campo == null) {
                            throw new AtributoExcepcion();
                        }
                        campo.setAccessible(true);
//                    System.out.println(campo);

                        if (evaluarElemento(campo.get(elementoBusqueda), (E) campo.get(array[i]), false)) {
                            listaConBusqueda.insertar(array[i]);
                        }
                    }

                    if (evaluarElemento(elementoBusqueda, array[i], false)) {
                        listaConBusqueda.insertar(array[i]);
                    }
                }
            }
        }
        if (retornarPos) {
            return (ListaEnlazada<E>) listaDePos;
        } else {
            return listaConBusqueda;
        }
    }

    private Boolean evaluarElemento(Object elementoBusqueda, Object elementoActual, Boolean stringExacto) {
        if (Utilidades.isNumber(elementoBusqueda.getClass())) {
            if (((Number) elementoActual).doubleValue() == ((Number) elementoBusqueda).doubleValue()) {
                return true;
            }
        } else if (Utilidades.isString(elementoBusqueda.getClass())) {
            if (stringExacto) {
                if (((String) elementoActual).contains(((String) elementoBusqueda))) {
                    return true;
                }
            } else {
                if (((String) elementoActual).toLowerCase().contains(((String) elementoBusqueda).toLowerCase())) {
                    return true;
                }
            }
        } else if (Utilidades.isBoolean(elementoBusqueda.getClass())) {
            if (((Boolean) elementoBusqueda).equals((Boolean) elementoActual)) {
                return true;
            }
        } else if (Utilidades.isCharacter(elementoBusqueda.getClass())) {
            if (((Character) elementoBusqueda).equals((Character) elementoActual)) {
                return true;
            }
        } else if (Utilidades.isEnum(elementoBusqueda.getClass())) {
            if (((Enum) elementoBusqueda).equals((Enum) elementoActual)) {
                return true;
            }
        }
        return false;
    }

    // -------------------------------- BUSQUEDA BINARIA -------------------------------
    public Object busquedaBinaria(Object elementoBusqueda, Boolean retornarPos, String atributo) throws Exception {
        if (elementoBusqueda != null) {
            ListaEnlazada<E> aux = ordenacionShell(1, atributo);
            E[] array = aux.toArray();
            if (array.length < 0 || array == null) {
                throw new BusquedaNulaException("El array esta vacio o no definido");
            }
            int i = 0;
            int j = array.length - 1;
            int p = (i + j) / 2;
            return dividirArreglo(i, j, p, array, elementoBusqueda, atributo, retornarPos);
        } else {
            throw new BusquedaNulaException("El elemento a buscar es nulo");
        }
    }

    private Object dividirArreglo(int i, int j, int p, E[] array, Object elementoBusqueda, String atributo, Boolean retornarPos) throws Exception {

        Object pTemp = array[p];
        Object elementoBusquedaTemp = elementoBusqueda;

        if (Utilidades.isObject(array[0].getClass())) {
            Class clazz = getCabecera().getDato().getClass();
            Field campo = Utilidades.obtenerAtributo(clazz, atributo);
            if (campo == null) {
                throw new AtributoExcepcion();
            }
            campo.setAccessible(true);

            pTemp = campo.get(array[p]);
            elementoBusquedaTemp = campo.get(elementoBusqueda);
        }

        if (evaluarElemento(elementoBusquedaTemp, pTemp, true)) {
            return (retornarPos == true) ? p : array[p];
        } else if (i == p) {
            boolean res = evaluarElemento(elementoBusquedaTemp, obtenerValorAtributo(array[j], atributo), true);
            if (res == true) {
                return (retornarPos == true) ? j : array[j];
            } else {
                return null;
            }
        } else if (Utilidades.isNumber(elementoBusquedaTemp.getClass())) {
            if (((Number) elementoBusquedaTemp).doubleValue() > ((Number) pTemp).doubleValue()) {
                i = p;
            } else {
                j = p;
            }
            p = (i + j) / 2;
//            return dividirArreglo(i, j, p, array, elementoBusqueda, atributo, retornarPos);

        } else if (Utilidades.isString(elementoBusquedaTemp.getClass())) {
            if (((String) elementoBusquedaTemp).compareToIgnoreCase((String) pTemp) > 0) {
                i = p;
            } else {
                j = p;
            }
            p = (i + j) / 2;
//            return dividirArreglo(i, j, p, array, elementoBusqueda, atributo, retornarPos);
        } else if (Utilidades.isCharacter(elementoBusquedaTemp.getClass())) {
            if (((Character) elementoBusquedaTemp).toString().compareToIgnoreCase(((Character) pTemp).toString()) > 0) {
                i = p;
            } else {
                j = p;
            }
            p = (i + j) / 2;
//            return dividirArreglo(i, j, p, array, elementoBusqueda, atributo, retornarPos);
        } else if (Utilidades.isEnum(elementoBusquedaTemp.getClass())) {
            if (((Enum) elementoBusquedaTemp).compareTo(((Enum) pTemp)) > 0) {
                i = p;
            } else {
                j = p;
            }
            p = (i + j) / 2;
        }
        return dividirArreglo(i, j, p, array, elementoBusqueda, atributo, retornarPos);
//        return null;
    }

    // ---------------------------------- BUSQUEDA BINARIA LINEAL ------------------------
    public ListaEnlazada<Object> busquedaBinariaLineal(Object elementoBusqueda, Boolean retornarPos, String atributo) throws Exception {
        if (elementoBusqueda != null) {
            ListaEnlazada<E> aux = ordenacionShell(1, atributo);
            E[] array = aux.toArray();
            if (array.length < 0 || array == null) {
                throw new BusquedaNulaException("El array esta vacio o no definido");
            }
            int i = 0;
            int j = array.length - 1;
            int p = (i + j) / 2;
            return dividirArregloBinarioLineal(i, j, p, array, elementoBusqueda, atributo, retornarPos);
        } else {
            throw new BusquedaNulaException("El elemento a buscar es nulo");
        }
    }

    private ListaEnlazada<Object> dividirArregloBinarioLineal(int i, int j, int p, E[] array, Object elementoBusqueda, String atributo, Boolean retornarPos) throws Exception {

        Object pTemp = array[p];
        Object elementoBusquedaTemp = elementoBusqueda;
        ListaEnlazada<Object> resLista = new ListaEnlazada<>();

        if (Utilidades.isObject(array[0].getClass())) {
            Class clazz = getCabecera().getDato().getClass();
            Field campo = Utilidades.obtenerAtributo(clazz, atributo);
            if (campo == null) {
                throw new AtributoExcepcion();
            }
            campo.setAccessible(true);

            pTemp = campo.get(array[p]);
            elementoBusquedaTemp = campo.get(elementoBusqueda);
        }

        if (evaluarElemento(elementoBusquedaTemp, pTemp, false)) {
            if (retornarPos == true) {
                resLista.insertar(p);
            } else {
                resLista.insertar(array[p]);
            }
            ListaEnlazada<Object> aux = new ListaEnlazada<>();
            aux.toListDesdeRango(i, j, array);
            return aux.busquedaLineal(elementoBusqueda, retornarPos, atributo);
//            return resLista;
        } //        else if (i == p) {
        //            boolean res = evaluarElemento(elementoBusquedaTemp, obtenerValorAtributo(array[j], atributo), false);
        //            if (res == true) {
        //                if (retornarPos == true) {
        //                    resLista.insertar(j);
        //                } else {
        //                    resLista.insertar(array[j]);
        //                }
        //            } else {
        //                return null;
        //            }
        //        }
        else if (Utilidades.isNumber(elementoBusquedaTemp.getClass())) {
            if (((Number) elementoBusquedaTemp).doubleValue() > ((Number) pTemp).doubleValue()) {
                i = p;
            } else {
                j = p;
            }
            p = (i + j) / 2;
//            return dividirArreglo(i, j, p, array, elementoBusqueda, atributo, retornarPos);

        } else if (Utilidades.isString(elementoBusquedaTemp.getClass())) {
            if (((String) elementoBusquedaTemp).compareToIgnoreCase((String) pTemp) > 0) {
                i = p;
            } else {
                j = p;
            }
            p = (i + j) / 2;
//            return dividirArreglo(i, j, p, array, elementoBusqueda, atributo, retornarPos);
        } else if (Utilidades.isCharacter(elementoBusquedaTemp.getClass())) {
            if (((Character) elementoBusquedaTemp).toString().compareToIgnoreCase(((Character) pTemp).toString()) > 0) {
                i = p;
            } else {
                j = p;
            }
            p = (i + j) / 2;
//            return dividirArreglo(i, j, p, array, elementoBusqueda, atributo, retornarPos);
        } else if (Utilidades.isEnum(elementoBusquedaTemp.getClass())) {
            if (((Enum) elementoBusquedaTemp).compareTo(((Enum) pTemp)) > 0) {
                i = p;
            } else {
                j = p;
            }
            p = (i + j) / 2;
        }

        if (j - i <= 10) {
//            System.out.println("entrooo");
            ListaEnlazada<Object> aux = new ListaEnlazada<>();
            aux.toListDesdeRango(i, j, array);
            return aux.busquedaLineal(elementoBusqueda, retornarPos, atributo);
        } else {
            return dividirArregloBinarioLineal(i, j, p, array, elementoBusqueda, atributo, retornarPos);
        }

//        return null;
    }

    private Object obtenerValorAtributo(Object objeto, String atributo) throws Exception {
        if (Utilidades.isObject(objeto.getClass())) {
            Class clazz = getCabecera().getDato().getClass();
            Field campo = Utilidades.obtenerAtributo(clazz, atributo);
            if (campo == null) {
                throw new AtributoExcepcion();
            }
            campo.setAccessible(true);
            objeto = campo.get(objeto); //quizas error
        }

        return objeto;
    }

    public ListaEnlazada<E> concatenarListas(ListaEnlazada<E> listaB) throws Exception {
        E[] arrayB = listaB.toArray();

        for (int i = 0; i < arrayB.length; i++) {
            this.insertar(listaB.obtener(i));
        }
        return this;
    }

    public ListaEnlazada<E> insertarArreglo(E[] array) throws Exception {
        for (int i = 0; i < array.length; i++) {
            this.insertar(array[i]);
        }
        return this;
    }

    public NodoLista<E> getCabecera() {
        return cabecera;
    }

    public void setCabecera(NodoLista<E> cabecera) {
        this.cabecera = cabecera;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }
}
