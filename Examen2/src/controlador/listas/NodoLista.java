package controlador.listas;

public class NodoLista<E> {
    //Se pone E, porque es un ELEMENTO
    //E reemplaza el tipo de dato Object

    private E dato; //Primer elemento del TDA de la lista
    private NodoLista<E> siguiente; //Segundo elemento del TDA de la lista
    private NodoLista<E> anterior;

    public NodoLista() {
        this.dato = null;
        this.siguiente = null;
        this.anterior = null;
    }

    public NodoLista(E dato, NodoLista<E> siguiente) {
        this.dato = dato;
        this.siguiente = siguiente;
    }

    public NodoLista(E dato, NodoLista<E> siguiente, NodoLista<E> anterior) {
        this.dato = dato;
        this.siguiente = siguiente;
        this.anterior = anterior;
    }

    public E getDato() {
        return dato;
    }

    public void setDato(E dato) {
        this.dato = dato;
    }

    public NodoLista<E> getSiguiente() {
        return siguiente;
    }

    public void setSiguiente(NodoLista<E> siguiente) {
        this.siguiente = siguiente;
    }

    public NodoLista<E> getAnterior() {
        return anterior;
    }

    public void setAnterior(NodoLista<E> anterior) {
        this.anterior = anterior;
    }

}
