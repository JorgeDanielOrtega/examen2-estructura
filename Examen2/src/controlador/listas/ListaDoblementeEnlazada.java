/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.listas;

import controlador.listas.excepciones.ListIsVoidException;
import controlador.listas.excepciones.ListOutLimitException;



public class ListaDoblementeEnlazada<E> {

    private NodoLista<E> cabeza;
    private NodoLista<E> cola;
    private Integer size;

    public ListaDoblementeEnlazada() {
        this.size = 0;
        this.cabeza = null;
        this.cola = null;
    }

    public Boolean isVoid() {
        return cabeza == null;
    }

    public void insert(E dato) {
        NodoLista<E> nodo = new NodoLista<>(dato, null, null);
        if (isVoid()) {
            cabeza = nodo;
            cola = cabeza;
            System.out.println("xd1");
        } else {
            System.out.println("xd2");
            NodoLista<E> aux = cabeza;
//            Integer limit = (size == 1)? size - 1 :  ;
            for (int i = 0; i < size - 1; i++) {
                aux = aux.getSiguiente();
            }
//            while (aux.getSiguiente() != null) {
//                aux =aux.getSiguiente();
//            }            
            aux.setSiguiente(nodo);
            nodo.setAnterior(aux);
            cola = nodo;
        }
        size++;
    }

    public void insertarCabecera(E dato) {
        if (isVoid()) {
            insert(dato);
        } else {
            NodoLista<E> nodo = new NodoLista<>(dato, null, null);
//            NodoLista<E> aux = cabeza;
            nodo.setSiguiente(cabeza);
            cabeza.setAnterior(nodo);
            cabeza = nodo;
            size++;
        }
    }

    public void insertarByPosition(E dato, Integer pos) throws ListOutLimitException {
//        if (isVoid() || pos == size - 1) {
//            insert(dato);
//        } 
        if (isVoid()) {
            insert(dato);
        } else if (pos == 0) {
            insertarCabecera(dato);
        } else if (pos > 0 && pos < size) {
            NodoLista<E> aux = cabeza;
            for (int i = 0; i < pos - 1; i++) {
                aux = aux.getSiguiente();
            }
            NodoLista<E> nodo = new NodoLista<>(dato, aux.getSiguiente(), aux);
            aux.getSiguiente().setAnterior(nodo);
            aux.setSiguiente(nodo);
            size++;
        } else {
            throw new ListOutLimitException(pos);
        }
    }

    public void print() {
        NodoLista<E> aux = cabeza;

        System.out.println("-------------- LISTA DOBLEMENTE ENLAZADA ------------------");
        while (aux != null) {
            String siguiente = aux.getSiguiente() != null ? aux.getSiguiente().getDato().toString() : "null";
            String anterior = aux.getAnterior() != null ? aux.getAnterior().getDato().toString() : "null";
            System.out.println("\t" + aux.getDato() + "\tSiguiente: " + siguiente + "\tAnterior: " + anterior);
            aux = aux.getSiguiente();
        }
        System.out.println("-----------------------------------------------------------");
    }

    public E obtener(Integer pos) throws ListOutLimitException, ListIsVoidException {
        if (isVoid()) {
            throw new ListIsVoidException();
        } else if (pos >= 0 && pos < size) {
            NodoLista<E> aux = cabeza;
            for (int i = 0; i < pos; i++) {
                aux = aux.getSiguiente();
            }
            return aux.getDato();
        } else {
            throw new ListOutLimitException(pos);
        }
    }

    public void modificar(E dato, Integer pos) throws ListIsVoidException, ListOutLimitException {
        if (isVoid()) {
            throw new ListIsVoidException();
        } else if (pos >= 0 && pos < size) {
            NodoLista<E> aux = cabeza;
            for (int i = 0; i < pos; i++) {
                aux = aux.getSiguiente();
            }
            aux.setDato(dato);
        } else {
            throw new ListOutLimitException(pos);
        }
    }

    public void eliminarUltimo() throws ListIsVoidException {
        if (isVoid()) {
            throw new ListIsVoidException();
        } else if (size == 1) {
            eliminarCabeza();
        } else {
            NodoLista<E> aux = cola;
            aux.getAnterior().setSiguiente(null);
            cola = aux.getAnterior();
            aux.setAnterior(null);
            aux.setDato(null);
            aux = null;
            size--;
        }
    }

    public void eliminarCabeza() throws ListIsVoidException {
        if (isVoid()) {
            throw new ListIsVoidException();
        } else if (size == 1) {
            cabeza = null;
        } else {
            NodoLista<E> aux = cabeza;
            aux.getSiguiente().setAnterior(null);
            cabeza = aux.getSiguiente();
            aux.setSiguiente(null);
            aux.setDato(null);
            aux = null;
        }
        size--;
    }

    public void eliminarByPosition(Integer pos) throws ListIsVoidException, ListOutLimitException {
        if (isVoid()) {
            throw new ListIsVoidException();
        } else if (pos == size - 1) {
            eliminarUltimo();
        } else if (pos == 0) {
            eliminarCabeza();
        } else if (pos > 0 && pos < size) {
            NodoLista<E> aux = cabeza;
            for (int i = 0; i < pos; i++) {
                aux = aux.getSiguiente();
            }
            aux.getSiguiente().setAnterior(aux.getAnterior());
            aux.getAnterior().setSiguiente(aux.getSiguiente());
            aux.setSiguiente(null);
            aux.setAnterior(null);
            aux.setDato(null);
            aux = null;
            size--;

        } else {
            throw new ListOutLimitException(pos);
        }
    }

    public NodoLista<E> getCabeza() {
        return cabeza;
    }

    public void setCabeza(NodoLista<E> cabeza) {
        this.cabeza = cabeza;
    }

    public NodoLista<E> getCola() {
        return cola;
    }

    public void setCola(NodoLista<E> cola) {
        this.cola = cola;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

}
