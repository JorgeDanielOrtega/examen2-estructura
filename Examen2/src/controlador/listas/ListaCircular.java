/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.listas;

import controlador.listas.excepciones.ListIsVoidException;
import controlador.listas.excepciones.ListOutLimitException;

public class ListaCircular<E> {

    NodoLista<E> cabeza;
    NodoLista<E> cola;
    Integer size;

    public ListaCircular() {
        this.size = 0;
        this.cabeza = null;
        this.cola = null;
    }

    public Boolean isVoid() {
        return cabeza == null;
    }

    public void insertar(E dato) {
        NodoLista<E> nodo = new NodoLista<E>(dato, null, null);
        if (isVoid()) {
            cabeza = nodo;
            cola = cabeza;
        } else {
            NodoLista<E> aux = cabeza;
            for (int i = 0; i < size - 1; i++) {
                aux = aux.getSiguiente();
            }
            aux.setSiguiente(nodo);
            nodo.setAnterior(aux);
            cola = nodo;
        }
        cabeza.setAnterior(cola);
        cola.setSiguiente(cabeza);
        size++;

    }

    public void insertarCabecera(E dato) {
        if (isVoid()) {
            insertar(dato);
        } else {
            NodoLista<E> nodo = new NodoLista<E>(dato, cabeza, cola);
            cabeza.setAnterior(nodo);
            cabeza = nodo;
            cola.setSiguiente(cabeza);
            size++;
        }

    }

    public void insertarByPosition(E dato, Integer pos) {
        if (isVoid()) {
            insertar(dato);
        } else if (pos == 0) {
            insertarCabecera(dato);
        } else if (pos > 0 && pos < size) {
            NodoLista<E> aux = cabeza;
            Integer limit = (pos == 1) ? 0 : pos - 1;
            for (int i = 0; i < limit; i++) {
                aux = aux.getSiguiente();
            }
            NodoLista<E> nodo = new NodoLista<E>(dato, null, null);
            aux.getSiguiente().setAnterior(nodo);
            nodo.setSiguiente(aux.getSiguiente());
            aux.setSiguiente(nodo);
            nodo.setAnterior(aux);
            size++;
        }

    }

    public void print() {
        NodoLista<E> aux = getCabeza();
        System.out.println("-------------- LISTA CIRCULAR ------------------");
        for (int i = 0; i < getSize(); i++) {
            System.out.println("\t" + aux.getDato() + "\tSiguiente: " + aux.getSiguiente().getDato() + "\tAnterior: " + aux.getAnterior().getDato());
            aux = aux.getSiguiente();
        }
        System.out.println("-----------------------------------------------------------");
    }
    
    public E obtener(Integer pos) throws ListOutLimitException, ListIsVoidException {
         if (isVoid()) {
            throw new ListIsVoidException();
        } else if (pos >= 0 && pos < size) {
            NodoLista<E> aux = cabeza;
            for (int i = 0; i < pos; i++) {
                aux = aux.getSiguiente();
            }
            return aux.getDato();
        } else {
            throw new ListOutLimitException(pos);
        }
    }

    public void modificar(E dato, Integer pos) throws ListIsVoidException, ListOutLimitException {
        if (isVoid()) {
            throw new ListIsVoidException();
        } else if (pos >= 0 && pos < size) {
            NodoLista<E> aux = cabeza;
            for (int i = 0; i < pos; i++) {
                aux = aux.getSiguiente();
            }
            aux.setDato(dato);
        } else {
            throw new ListOutLimitException(pos);
        }
    }

    public void eliminarCola() throws ListIsVoidException {
        if (isVoid()) {
            throw new ListIsVoidException();
        } else if (size == 1) {
            eliminarCabeza();
        } else {
            NodoLista<E> aux = cola;
            aux.getAnterior().setSiguiente(cabeza);
            cola = aux.getAnterior();
            aux.setAnterior(null);
            aux.setSiguiente(null);
            aux.setDato(null);
            aux = null;
            cabeza.setAnterior(cola);
            size--;
        }
    }

    public void eliminarCabeza() throws ListIsVoidException {
        if (isVoid()) {
            throw new ListIsVoidException();
        } else if (size == 1) {
            cabeza = null;
        } else {
            NodoLista<E> aux = cabeza;
            aux.getSiguiente().setAnterior(cola);
            cabeza = aux.getSiguiente();
            aux.setSiguiente(null);
            aux.setAnterior(null);
            aux.setDato(null);
            aux = null;
            cola.setSiguiente(cabeza);
        }
        size--;
    }

    public void eliminarByPosition(Integer pos) throws ListIsVoidException, ListOutLimitException {
 if (isVoid()) {
            throw new ListIsVoidException();
        } else if (pos == size - 1) {
            eliminarCola();
        } else if (pos == 0) {
            eliminarCabeza();
        } else if (pos > 0 && pos < size) {
            NodoLista<E> aux = cabeza;
            for (int i = 0; i < pos; i++) {
                aux = aux.getSiguiente();
            }
            aux.getSiguiente().setAnterior(aux.getAnterior());
            aux.getAnterior().setSiguiente(aux.getSiguiente());
            aux.setSiguiente(null);
            aux.setAnterior(null);
            aux.setDato(null);
            aux = null;
            size--;

        } else {
            throw new ListOutLimitException(pos);
        }
    }

    public NodoLista<E> getCabeza() {
        return cabeza;
    }

    public void setCabeza(NodoLista<E> cabeza) {
        this.cabeza = cabeza;
    }

    public NodoLista<E> getCola() {
        return cola;
    }

    public void setCola(NodoLista<E> cola) {
        this.cola = cola;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

}
