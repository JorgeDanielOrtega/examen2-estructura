/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import controlador.listas.ListaEnlazada;
import modelo.Cliente;
import modelo.Factura;
import modelo.Retencion;
import modelo.TipoServicio;

/**
 *
 * @author daniel
 */
public class ctrlFacturas {

    private ListaEnlazada<Factura> listaFacturas = new ListaEnlazada<>();
    private ListaEnlazada<Retencion> listaRetenciones = new ListaEnlazada<>();

    public ListaEnlazada<Factura> getFacturaList() {
        return listaFacturas;
    }
    
    public ListaEnlazada<Retencion> getRetencionesList() {
        return listaRetenciones;
    }

    public void setFacturaList(ListaEnlazada<Factura> listaFacturas) {
        this.listaFacturas = listaFacturas;
    }

    public Retencion anadirFactura(String nombre, String apellido, String cedula, Float total, TipoServicio tipoServicio) {
        Cliente cliente = new Cliente();
        cliente.setNombre(nombre);
        cliente.setApellido(apellido);
        cliente.setCedula(cedula);

        Factura nueva = new Factura();
        nueva.setCliente(cliente);
        nueva.setTotal(total);
        nueva.setTipoServicio(tipoServicio);

        Retencion retencion = new Retencion();
        retencion.setFactura(nueva);
        
        listaFacturas.insertar(nueva);
        listaRetenciones.insertar(retencion);

        System.out.println(nueva.toString());
        System.out.println("Total retenciones: " + listaRetenciones.getUltimaPosicionOcupada());
        System.out.println("Total facturas: " + listaFacturas.getUltimaPosicionOcupada());
        
        utilidades.Utilidades.guardarJson(this, "Facturas");

        return retencion;
    }

}
