/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.pilas;

import controlador.listas.excepciones.ListOutLimitException;
import controlador.listas.ListaEnlazada;

/**
 *
 * @author daniel
 */
public class Cola<E> extends ListaEnlazada<E> {

    private Integer cima; // En las colas el limite se llama cima

    public Cola() {
    }

    public Cola(Integer cima) {
        this.cima = cima;
    }

    public Boolean isFull() {
        return getSize() == cima;
    }

    public void queque(E dato) throws StackOverflowError {
        if (isFull()) {
            throw new StackOverflowError();
        }
        
        try {

            insertar(dato);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
    
    public void deque(){
        try {
            eliminarCabecera();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
    public void dequeByPosition(Integer pos){
        try {
            if (pos < 0 || pos >= cima) {
                throw new ListOutLimitException(pos);
            }

            for (int i = 0; i < pos + 1; i++) {
                eliminarCabecera();
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
    

}
