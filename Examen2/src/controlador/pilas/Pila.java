/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.pilas;

import controlador.listas.excepciones.ListIsVoidException;
import controlador.listas.excepciones.ListOutLimitException;
import controlador.listas.ListaEnlazada;
import controlador.pilas.exceptions.StackFullException;

/**
 *
 * @author daniel
 */
public class Pila<E> extends ListaEnlazada<E> {

    private Integer tope; //Normalmente se implementan listas finitas // En las pilas el limite se llama cima

    public Pila() {
    }

    public Pila(Integer limite) {
        this.tope = limite;
    }

    public Boolean isFull() {
        return tope == getSize();
    }

    public void push(E dato) throws StackFullException {
        if (isFull()) {
            throw new StackFullException();
        } else {
            insertar(dato);
        }
    }

    public void pop() {
        try {
            eliminarUltimo();
        } catch (ListIsVoidException e) {
            System.err.println(e.getMessage());
        }
    }

    public void popByPosition(Integer pos) throws ListOutLimitException {
        try {
            if (pos < 0 || pos >= tope) {
                throw new ListOutLimitException(pos);
            }
            Integer tempSize = getSize();
            for (int i = 0; i < tempSize - pos; i++) {
                eliminarUltimo();
            }

        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

}
