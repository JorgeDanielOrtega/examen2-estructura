/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.pilas.exceptions;

/**
 *
 * @author daniel
 */
public class StackFullException extends Exception{

    public StackFullException() {
        super("La pila esta llena");
    }
    
    
    public StackFullException(String msg){
        super(msg);
    }
}
